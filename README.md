# Работа с микропроцессором фирмы Миландер *1986BE92Y*.

Для просмотра документов можно перейти по вкладке [Wiki](https://gitlab.com/zakolesnik/uart_dma/wikis/home)

или скачать документацию отсюда: 
1. [Техническое задание оформление РЖД](https://gitlab.com/zakolesnik/uart_dma/raw/master/%D0%A2%D0%97%20%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D1%8C%20DMA%20%D0%B2%20UART_.doc)